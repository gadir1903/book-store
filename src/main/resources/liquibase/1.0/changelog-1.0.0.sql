-- liquibase formatted sql

-- changeset liquibase:1
--comment authors table created
CREATE TABLE authors
(
    id       int,
    name     varchar,
    surname  varchar,
    username varchar,
    age      int,
    email    varchar,
    password varchar,
    role     varchar,
    primary key (id)
)
