-- liquibase formatted sql

-- changeset liquibase:2
--comment create many to many relation

CREATE TABLE student_books
(
    student_id bigint,
    book_id    bigint,
    FOREIGN KEY (student_id) REFERENCES students (id),
    FOREIGN KEY (book_id) REFERENCES books (id),
    PRIMARY KEY (student_id, book_id)
);