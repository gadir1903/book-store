-- liquibase formatted sql

-- changeset liquibase:1
--comment added author_id
ALTER TABLE books
    ADD FOREIGN KEY (author_id) REFERENCES authors (id);