-- liquibase formatted sql

-- changeset liquibase:2
--comment students table created
CREATE TABLE students
(
    id       int,
    name     varchar,
    surname  varchar,
    username varchar,
    age      int,
    email    varchar,
    password varchar,
    role     varchar,
    primary key (id)
)
