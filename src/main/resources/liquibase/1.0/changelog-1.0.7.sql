-- liquibase formatted sql

-- changeset liquibase:2
--comment added readers
ALTER TABLE books
    ADD COLUMN readers VARCHAR;