-- liquibase formatted sql

-- changeset liquibase:2
--comment added booksRead
ALTER TABLE students
    ADD COLUMN booksRead VARCHAR;