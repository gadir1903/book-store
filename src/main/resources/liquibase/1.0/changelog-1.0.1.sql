-- liquibase formatted sql

-- changeset liquibase:1
--comment books table created
CREATE TABLE books
(
    id   int,
    name varchar,
    primary key (id)
)