package az.ingress.msauthor.exception;

public class AuthorNotAuthorizedException extends RuntimeException {
    public AuthorNotAuthorizedException(String message) {
        super(message);
    }
}
