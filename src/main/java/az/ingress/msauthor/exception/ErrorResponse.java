package az.ingress.msauthor.exception;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class ErrorResponse {
    String message;

    public static ErrorResponse of(Exception e) {
        return new ErrorResponse(e.getMessage());
    }

    public static ErrorResponse of(String errorMessage) {
        return new ErrorResponse(errorMessage);
    }
}
