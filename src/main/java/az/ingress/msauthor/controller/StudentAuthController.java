package az.ingress.msauthor.controller;

import az.ingress.msauthor.model.RegisterRequest;
import az.ingress.msauthor.model.StudentRegisterResponse;
import az.ingress.msauthor.service.StudentAuthenticationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student-auth")
@RequiredArgsConstructor
public class StudentAuthController {
    private final StudentAuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity<StudentRegisterResponse> register(@RequestBody @Valid RegisterRequest request) {
        return ResponseEntity.ok(service.register(request));

    }
}
