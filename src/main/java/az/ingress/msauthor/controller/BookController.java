package az.ingress.msauthor.controller;

import az.ingress.msauthor.dao.entity.AuthorEntity;
import az.ingress.msauthor.dao.entity.BookEntity;
import az.ingress.msauthor.dao.entity.StudentEntity;
import az.ingress.msauthor.dao.repository.AuthorRepository;
import az.ingress.msauthor.dao.repository.BookRepository;
import az.ingress.msauthor.exception.UserNotFoundException;
import az.ingress.msauthor.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    @PostMapping("/create")
    public ResponseEntity<String> createBook(@RequestBody BookEntity bookEntity, @AuthenticationPrincipal UserDetails userDetails) {

        AuthorEntity author = authorRepository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new UserNotFoundException("Author not found"));
        bookService.createBook(bookEntity, author);
        return ResponseEntity.ok("Book created successfully... Notification was sent to the subscribers!");
    }

    @GetMapping("/getMyBooks")
    public ResponseEntity<List<BookEntity>> getMyBooks(@AuthenticationPrincipal UserDetails userDetails) {
        AuthorEntity author = authorRepository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new UserNotFoundException("Author not found"));
        List<BookEntity> myBooks = bookRepository.findByAuthor(author);
        return ResponseEntity.ok(myBooks);
    }

    @DeleteMapping("/deleteBook/{bookId}")
    public ResponseEntity<String> deleteBookById(@PathVariable Long bookId, @AuthenticationPrincipal UserDetails userDetails) {
        AuthorEntity author = authorRepository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new UserNotFoundException("Author not found"));
        bookService.deleteBookById(bookId, author);
        return ResponseEntity.ok("Book deleted successfully");
    }

    @GetMapping("/getAllBooks")
    public List<BookEntity> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/books/{studentId}")
    public ResponseEntity<List<BookEntity>> getBooksReadByStudent(@PathVariable Long studentId) {
        return bookService.getBooksReadByStudent(studentId);
    }

    @PostMapping("/{studentId}/books/{bookId}")
    public ResponseEntity<String> addBookToStudent(@PathVariable Long studentId, @PathVariable Long bookId) {
        return bookService.addBookToStudent(studentId,bookId);
    }
    @GetMapping("/readers/{bookId}")
    public ResponseEntity<List<StudentEntity>> getBookReaders(@PathVariable Long bookId) {
        return bookService.getBookReaders(bookId);
    }
}
