package az.ingress.msauthor.model.dto;

import az.ingress.msauthor.model.Role;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorProfileDto {
    Long id;
    String name;
    String surname;
    String username;
    Long age;
    String email;
    @Enumerated(EnumType.STRING)
    Role role;
}
