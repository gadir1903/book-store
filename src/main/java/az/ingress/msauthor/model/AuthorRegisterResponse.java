package az.ingress.msauthor.model;

import az.ingress.msauthor.dao.entity.AuthorEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorRegisterResponse {
    String name;
    String surname;
    String username;
    Long age;
    String email;
    String password;
    Role role;

    public static AuthorRegisterResponse buildRegisterDto(AuthorEntity authorEntity) {
        return AuthorRegisterResponse.builder()
                .name(authorEntity.getName())
                .surname(authorEntity.getSurname())
                .username(authorEntity.getUsername())
                .age(authorEntity.getAge())
                .email(authorEntity.getEmail())
                .password(authorEntity.getPassword())
                .role(authorEntity.getRole())
                .build();
    }
}
