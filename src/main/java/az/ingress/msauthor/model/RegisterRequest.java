package az.ingress.msauthor.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterRequest {
    @NotBlank(message = "Firstname is required")
    @Size(min = 3, message = "Firstname must be minimum 3 characters")
    String name;
    @NotBlank(message = "Lastname is required")
    @Size(min = 3, message = "Lastname must be minimum 3 characters")
    String surname;
    @NotBlank(message = "Username is required")
    @Size(min = 4, max = 20, message = "Username must be between 4 and 20 characters")
    String username;
    @NotBlank(message = "Email is required")
    @Email(message = "Invalid email format")
    String email;
    Long age;
    @NotBlank(message = "Password is required")
    @Size(min = 6, message = "Password must be at least 6 characters")
    String password;
    Role role;
}
