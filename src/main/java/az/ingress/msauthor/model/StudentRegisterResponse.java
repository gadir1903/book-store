package az.ingress.msauthor.model;

import az.ingress.msauthor.dao.entity.StudentEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentRegisterResponse {
    String name;
    String surname;
    String username;
    Long age;
    String email;
    String password;
    Role role;

    public static StudentRegisterResponse buildRegisterDto(StudentEntity studentEntity) {
        return StudentRegisterResponse.builder()
                .name(studentEntity.getName())
                .surname(studentEntity.getSurname())
                .username(studentEntity.getUsername())
                .age(studentEntity.getAge())
                .email(studentEntity.getEmail())
                .password(studentEntity.getPassword())
                .role(studentEntity.getRole())
                .build();
    }
}
