package az.ingress.msauthor.service;

import az.ingress.msauthor.dao.entity.AuthorEntity;
import az.ingress.msauthor.dao.entity.BookEntity;
import az.ingress.msauthor.dao.entity.StudentEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BookService {
    void createBook(BookEntity bookEntity, AuthorEntity author);

    void deleteBookById(Long id, AuthorEntity author);

    List<BookEntity> getAllBooks();
    ResponseEntity<List<BookEntity>> getBooksReadByStudent(Long id);

    ResponseEntity<String> addBookToStudent(Long studentId, Long bookId);

    ResponseEntity<List<StudentEntity>> getBookReaders(Long id);
}
