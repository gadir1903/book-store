package az.ingress.msauthor.service;

import az.ingress.msauthor.dao.entity.StudentEntity;
import az.ingress.msauthor.dao.repository.StudentRepository;
import az.ingress.msauthor.model.RegisterRequest;
import az.ingress.msauthor.model.StudentRegisterResponse;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public record StudentAuthenticationService(StudentRepository studentRepository,
                                           PasswordEncoder passwordEncoder,
                                           AuthenticationManager authenticationManager,
                                           MessageSource messageSource) {
    public StudentRegisterResponse register(RegisterRequest request) {
        var exist = studentRepository.findByUsername(request.getUsername()).isPresent();
        if (exist) {
            Locale locale = LocaleContextHolder.getLocale();
            Object[] args = new Object[1];
            args[0] = request.getUsername();
            String message = messageSource.getMessage("usernameAlreadyExist", args, locale);
            throw new RuntimeException(message);
        }
        var user = StudentEntity.builder()
                .name(request.getName())
                .surname(request.getSurname())
                .username(request.getUsername())
                .age(request.getAge())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .build();
        var authorEntity = studentRepository.save(user);
        return StudentRegisterResponse.buildRegisterDto(authorEntity);
    }
}
