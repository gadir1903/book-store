package az.ingress.msauthor.service.impl;

import az.ingress.msauthor.dao.entity.AuthorEntity;
import az.ingress.msauthor.dao.entity.BookEntity;
import az.ingress.msauthor.dao.entity.StudentEntity;
import az.ingress.msauthor.dao.repository.BookRepository;
import az.ingress.msauthor.dao.repository.StudentRepository;
import az.ingress.msauthor.exception.AuthorNotAuthorizedException;
import az.ingress.msauthor.exception.BookNotFoundException;
import az.ingress.msauthor.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final StudentRepository studentRepository;

    @Override
    public void createBook(BookEntity bookEntity, AuthorEntity author) {
        BookEntity book = BookEntity.builder().author(author).name(bookEntity.getName()).build();
        bookRepository.save(book);
    }

    @Override
    public void deleteBookById(Long id, AuthorEntity author) {
        BookEntity book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException("Book Not Found"));
        if (!book.getAuthor().equals(author)) {
            throw new AuthorNotAuthorizedException("Author not authorized");
        }
        bookRepository.delete(book);
    }

    @Override
    public List<BookEntity> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public ResponseEntity<List<BookEntity>> getBooksReadByStudent(Long id) {
        StudentEntity student = studentRepository.findById(id).orElse(null);
        if (student == null) {
            return ResponseEntity.notFound().build();
        }
        List<BookEntity> booksRead = student.getBooksRead();
        return ResponseEntity.ok(booksRead);
    }

    @Override
    public ResponseEntity<String> addBookToStudent(Long studentId, Long bookId) {
        StudentEntity student = studentRepository.findById(studentId).orElse(null);
        BookEntity book = bookRepository.findById(bookId).orElse(null);

        if (student != null && book != null) {
            List<BookEntity> booksRead = student.getBooksRead();
            if (booksRead == null) {
                booksRead = new ArrayList<>();
            }

            booksRead.add(book);
            student.setBooksRead(booksRead);
            studentRepository.save(student);

            return ResponseEntity.ok("Book added to the student's list of books read.");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<List<StudentEntity>> getBookReaders(Long bookId) {
        BookEntity book = bookRepository.findById(bookId).orElse(null);

        if (book == null) {
            return ResponseEntity.notFound().build();
        }

        List<StudentEntity> readers = book.getReaders();
        return ResponseEntity.ok(readers);
    }
}
