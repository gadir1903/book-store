package az.ingress.msauthor.service;

import az.ingress.msauthor.dao.entity.AuthorEntity;
import az.ingress.msauthor.dao.repository.AuthorRepository;
import az.ingress.msauthor.exception.UsernameAlreadyExist;
import az.ingress.msauthor.model.AuthenticationRequest;
import az.ingress.msauthor.model.RegisterRequest;
import az.ingress.msauthor.model.AuthorRegisterResponse;
import az.ingress.msauthor.model.AuthenticationResponse;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public record AuthenticationService(AuthorRepository authorRepository,
                                    PasswordEncoder passwordEncoder,
                                    JwtService jwtService,
                                    AuthenticationManager authenticationManager,
                                    MessageSource messageSource) {
    public AuthorRegisterResponse register(RegisterRequest request) {
        var exist = authorRepository.findByUsername(request.getUsername()).isPresent();
        if (exist) {
            Locale locale = LocaleContextHolder.getLocale();
            Object[] args = new Object[1];
            args[0] = request.getUsername();
            String message = messageSource.getMessage("usernameAlreadyExist", args, locale);
            throw new UsernameAlreadyExist(message);
        }
        var user = AuthorEntity.builder()
                .name(request.getName())
                .surname(request.getSurname())
                .username(request.getUsername())
                .age(request.getAge())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .build();
        var authorEntity = authorRepository.save(user);
        return AuthorRegisterResponse.buildRegisterDto(authorEntity);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[1];
        args[0] = request.getUsername();
        String message = messageSource.getMessage("userNotFound", args, locale);
        var user = authorRepository.findByUsername(request.getUsername())
                .orElseThrow(() -> new RuntimeException(message));
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
