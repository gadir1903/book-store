package az.ingress.msauthor.dao.repository;

import az.ingress.msauthor.dao.entity.AuthorEntity;
import az.ingress.msauthor.dao.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
    List<BookEntity> findByAuthor(AuthorEntity author);
}
