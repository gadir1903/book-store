package az.ingress.msauthor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsBookStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsBookStoreApplication.class, args);
    }
}
