FROM openjdk:18
MAINTAINER baeldung.com
COPY build/libs/ms-author-0.0.1-SNAPSHOT.jar ms-author-0.0.1.jar
ENTRYPOINT ["java","-jar","/ms-author-0.0.1.jar"]